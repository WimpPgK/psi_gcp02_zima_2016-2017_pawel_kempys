public class Siec 
{
	
	Neuron n1,n2,n3;
	double [][] tab;
	int [] zwyciezcy;
	Zapis z1;
	int a,b;  // a: liczba rekordow uczacych , b:  liczba wartosci w rekordzie uczacym
	PrintWriter zapis;
	public Siec()
	{
		System.out.println("Obiekt klasy siec utworzony");
		
		 OdczytZPliku o1 = new OdczytZPliku();
		 tab = o1.zapisDoTab();
		 a = tab.length;
		 System.out.println(a);
		 b = 2;
		 zwyciezcy = new int[a];


		
		n1 = new Neuron(b, tab[0]);
		n2 = new Neuron(b, tab[0]);
		n3 = new Neuron(b, tab[0]);
		

		
		// petla w kt�rej 1 iteracja to 1 epoka
		for (int i = 0 ; i < 10 ; i++)
		{
			
			
			// petla po wszystkich rekordach wejsciowych
			for (int j = 0 ; j < a ; j++)
			{	
				pobierzRekord(j);
				dzialaj(j);
				
				
			}
			
			//n1.wypiszWagi();
		}

		
		

		zapiszOdpowiedz();
		
	}
	

	
//***********************************************
	void dzialaj(int n)		// arg to numer iteracji
	{
		
		double wsp = wspUczenia(n);
		
		if (n1.obliczOdleglosc() < n2.obliczOdleglosc() && n1.obliczOdleglosc() < n3.obliczOdleglosc())
		{
			System.out.println("1");
			zwyciezcy[n] = 1;
			n1.korektaWag(wsp);
		}
		else if (n2.obliczOdleglosc() < n1.obliczOdleglosc() && n2.obliczOdleglosc() < n3.obliczOdleglosc())
		{
			System.out.println("2");
			n2.korektaWag(wsp);
			zwyciezcy[n] = 2;
		}
		else
		{
			System.out.println("3");
			n3.korektaWag(wsp);
			zwyciezcy[n] = 3;
		}
		
		//n1.wypiszWagi();
		//n2.wypiszWagi();
		//n3.wypiszWagi();
	}
	
	
//***********************************************	
	double wspUczenia(int n)
	{
		int wsp = 0;
		
		if (n < 5)
		{
			return 0.6;
		}
		else if (n < 9)
		{
			return 0.6*0.5;
		}
		
		return 0.3*0.5;
		
	}

//***********************************************
	void pobierzRekord (int n)
	{
		n1.ustawWejscia(tab[n]);
		n2.ustawWejscia(tab[n]);
		n3.ustawWejscia(tab[n]);
	}
	

	
//***********************************************
	void zapiszOdpowiedz()
	{
		

		for (int i= 0 ; i < zwyciezcy.length ; i++)
		{
			System.out.println("Wygrywa: " + zwyciezcy[i]);
		}

		
		
		try {
			zapis = new PrintWriter("nazwa_pliku.txt");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    		try {
			zapis = new PrintWriter("wynikSieci.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    		
	    	zapis.println("Argumenty x: ");
			for (int i = 0 ; i < a ; i++)
			{
				zapis.println(Double.toString(tab[i][0]));
			}
			zapis.println();
			zapis.println();
			zapis.println("Argumenty y: ");
			for (int i = 0 ; i < a ; i++)
			{
				zapis.println(Double.toString(tab[i][1]));
			}
			
			
			
			zapis.println();
			zapis.println();
			zapis.println("Wynik sieci y: ");
			for (int i = 0 ; i < a ; i++)
			{
				zapis.println(Integer.toString(zwyciezcy[i]));
			}
			
			zapis.println("Wagi neuronu 1: " +  n1.waga[0] + " " + n1.waga[1]);
			zapis.println("Wagi neuronu 2: " +  n2.waga[0] + " " + n2.waga[1]);
			zapis.println("Wagi neuronu 3: " +  n3.waga[0] + " " + n3.waga[1]);
	      zapis.close();
		
	}
}


