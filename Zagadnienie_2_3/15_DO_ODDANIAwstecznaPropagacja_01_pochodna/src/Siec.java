/*******************************************************
 * 		void start(int numerRekorduUczacego) - oblicza odpowiedz sieci dla podanego w arg rekordu uczacego
 * 						Na poczatku ustawia wejscia do neuronow warstwy 0. Kazdy z nich moze miec tylko 1 wejscie
 * 						i kazdy po kolei dostaje kolejne elementy danego wektora uczacego. Nast�pnie w petli po kolei
 * 						kazdy neuron oblicza swoja odpowiedz zaczynajac od warstwy zerowej kt�ra zna swoje wejscia.
 * 						Po obliczeniu odpowiedzi warstwy zerowej zapisywane s� do tabeli wejsc dla warstwy 1.
 * 						Ca�o�c powtarza si� a� do ostatniej warstwy z 1 neuronem. Wypisuje on odpowiedz ca�ej sieci.
 * 
 * 		void inicjujTablice() - definiuje wszystkie potrzebne tablice danych (wejscia, wagi, wspBledu, wyniki) 
 * 								oraz wype�nia je zerami. Inicjuje losowe wagi dla ca�ej sieci. Pobiera z pliku dane uczace
 * 								i zapisuje z tablicy 2D "dane uczace" .
 * 
 */




import java.util.Random;

public class Siec 
{
	double [][] daneUczace; 	// przechowuje wszystkie rekordy uczace. [nr_rekordu][wartosci]

	public Neuron wejsciowy;
	public Neuron wyjsciowy;
	public Neuron [] ukryty;
	
	double wspBleduWejsciowy;
	double []wspBleduUkryty;
	double wspBleduWyjsciowy;
	double bladSredniokwadratowy;
	
	double wynikWejsciowy;
	double []wynikUkryty;
	double wynikWyjsciowy;
	
	double pochodnaFunkcjiWyjsciowy;
	double []pochodnaFunkcjiUkryty;
	double pochodnaFunkcjiWejsciowy;
	
	
	double MAPE;
	double pomMAPE;

	
	Siec ()
	{
		wejsciowy = new Neuron(1);
		ukryty = new Neuron [3];
		ukryty[0] = new Neuron(1);
		ukryty[1] = new Neuron(1);
		ukryty[2] = new Neuron(1);
		wyjsciowy = new Neuron (3);
		
		inicjujDane();
		
	}
	
	
	
	
	
	
	
	// ********************************************************************
	void inicjujDane()	// tworzy wszelkie tablice zadeklarowane u gory i inicjuje je zerami*    (* - wagi inicjuje liczbami pseudolosowymi) 
	{	
		
		//wyszukuje ile nertonow jest w warstwie z najwieksza liczba neuronow
		
		OdczytWejsc o1 = new OdczytWejsc(daneUczace);		// pobieramy dane uczace i zapisujemy do tab
		daneUczace = o1.zwroc();
		Random rand = new Random();
		wejsciowy.waga[0] = rand.nextDouble();
		ukryty[0].waga[0] = rand.nextDouble();
		ukryty[1].waga[0] = rand.nextDouble();
		ukryty[2].waga[0] = rand.nextDouble();
		wyjsciowy.waga[0] = rand.nextDouble();
		wyjsciowy.waga[1] = rand.nextDouble();
		wyjsciowy.waga[2] = rand.nextDouble();
		
		

		MAPE = 0;
		pomMAPE = 0;
		bladSredniokwadratowy = 0;
		
		
		
//		wejsciowy.waga[0] = 0.1;
//		ukryty[0].waga[0] = 0.4;
//		ukryty[1].waga[0] = 0.3;
//		ukryty[2].waga[0] = 0.7;
//		wyjsciowy.waga[0] = 0.2;
//		wyjsciowy.waga[1] = 0.9;
//		wyjsciowy.waga[2] = 0.5;
//		
		
		
		wspBleduWejsciowy = 0;
		wspBleduUkryty = new double[3];
		wspBleduUkryty[0] = 0;
		wspBleduUkryty[1] = 0;
		wspBleduUkryty[2] = 0;
		
		wynikWejsciowy = 0;
		wynikUkryty = new double[3];
		wynikUkryty[0] = 0;
		wynikUkryty[1] = 0;
		wynikUkryty[2] = 0;
		
		
		pochodnaFunkcjiWejsciowy = 0;
		pochodnaFunkcjiUkryty = new double [3];
		pochodnaFunkcjiUkryty[0]=0;
		pochodnaFunkcjiUkryty[1]=0;
		pochodnaFunkcjiUkryty[2]=0;
		pochodnaFunkcjiWyjsciowy = 0;
		

	}
	
	void dzialaj()
	{	
		int n = 7000;
		bladSredniokwadratowy = 0;
		MAPE = 0;
		pomMAPE = 0;
		double sumaBledu = 0;
		
		for(int i = 0 ; i < n ; i++)	// petla po danych uczacych
		{
			wejsciowy.wejscie[0] = daneUczace[i][0];
			wynikWejsciowy = wejsciowy.obliczOdpowiedz();
			
			
			ukryty[0].wejscie[0] = wynikWejsciowy;
			ukryty[1].wejscie[0] = wynikWejsciowy;
			ukryty[2].wejscie[0] = wynikWejsciowy;
			

			wynikUkryty[0] = ukryty[0].obliczOdpowiedz();
			wynikUkryty[1] = ukryty[1].obliczOdpowiedz();
			wynikUkryty[2] = ukryty[2].obliczOdpowiedz();
			
			wyjsciowy.wejscie[0] = wynikUkryty[0];
			wyjsciowy.wejscie[1] = wynikUkryty[1];
			wyjsciowy.wejscie[2] = wynikUkryty[2];
			wynikWyjsciowy = wyjsciowy.obliczOdpowiedz();

		
			
			
			//********************************************
			//********************************************
			//**********  propagacja wsteczna  ***********
			double krok = 0.000001;
			
			wspBleduWyjsciowy = daneUczace[i][1] - wynikWyjsciowy;
			// System.out.println("wsy Bledu wyjsciowy:  "  + wspBleduWyjsciowy);

			bladSredniokwadratowy = wspBleduWyjsciowy*wspBleduWyjsciowy;
			sumaBledu += bladSredniokwadratowy;

			
			
			
			
			
					//    MAPE     ///
			pomMAPE = wspBleduWyjsciowy/daneUczace[i][1];
			if (pomMAPE < 0)
			{
				pomMAPE = -pomMAPE;
			}
			MAPE += pomMAPE;
			

			
			
			pochodnaFunkcjiWejsciowy = wejsciowy.pochodnaFAktywacjiDlaTegoNeuronu();
			pochodnaFunkcjiUkryty[0]=ukryty[0].pochodnaFAktywacjiDlaTegoNeuronu();
			pochodnaFunkcjiUkryty[1]=ukryty[1].pochodnaFAktywacjiDlaTegoNeuronu();
			pochodnaFunkcjiUkryty[2]=ukryty[2].pochodnaFAktywacjiDlaTegoNeuronu();
			pochodnaFunkcjiWyjsciowy = wyjsciowy.pochodnaFAktywacjiDlaTegoNeuronu();
		

			
			wspBleduUkryty[0] = wspBleduWyjsciowy * wyjsciowy.waga[0] * pochodnaFunkcjiUkryty[0];
			wspBleduUkryty[1] = wspBleduWyjsciowy * wyjsciowy.waga[1]* pochodnaFunkcjiUkryty[1];
			wspBleduUkryty[2] = wspBleduWyjsciowy * wyjsciowy.waga[2]* pochodnaFunkcjiUkryty[2];
			wspBleduWejsciowy = ((wspBleduUkryty[0] * ukryty[0].waga[0]) + (wspBleduUkryty[1] * ukryty[1].waga[0]) + (wspBleduUkryty[2] * ukryty[2].waga[0]))* pochodnaFunkcjiWejsciowy;


			wyjsciowy.aktualizuj(wspBleduWyjsciowy, krok);
			ukryty[0].aktualizuj(wspBleduUkryty[0], krok);
			ukryty[1].aktualizuj(wspBleduUkryty[1], krok);
			ukryty[2].aktualizuj(wspBleduUkryty[2], krok);
			wejsciowy.aktualizuj(wspBleduWejsciowy, krok);

		}
		
		// wynik bledu sredniokwadratowego
		sumaBledu = sumaBledu/n;
		
		
		// wynik MAPE
		MAPE = MAPE/n;
		
		
		//System.out.println(sumaBledu);
		System.out.println(MAPE);
		
	}
	
	
}
	
	
	
	
	