
public class Neuron 
{

	public double [] wejscie;
	public double [] waga;
	int n;					// liczba wejsc
	double pm;
	public Neuron(int n)
	{
		wejscie = new double[n];
		waga = new double[n];
		this.n = n;
	}
	

	//*************************************************************
	
	public double obliczOdpowiedz()
	{
		potecialMembranowy();

		double odpowiedz = funkcjaAktywacji(1);
		return odpowiedz;

	}
	

	//************************************************************
	private double funkcjaAktywacji(int parametr)		
	{
		
		if (parametr == 1)
		{
			return (1/(1+Math.pow(2.718281828459, -10*pm)));
		}
		if (parametr == 2)
		{
			if (pm >= 0) { return 1;}
			else { return -1;}
		}
		
		if (parametr == 3)
		{
			if (pm >= 0) { return 1;}
			else { return 0;}
		}
		
		
		return 0;
	}
	
	
	//*************************************************************
	private void potecialMembranowy() // suma wi*xi
	{
		pm = 0;
		for (int i = 0 ; i < n ; i++)
		{
			pm += waga[i]*wejscie[i];
			
		}
	}

	
	//*************************************************************
	public void aktualizuj(double wspBledu, double krok)
	{
		for (int i = 0 ; i < n ; i++)
		{
			waga[i] += krok * wspBledu * wejscie[i];
		}
		
	}
	
	
	
	
	
	//*************************************************************
	public double pochodnaFAktywacjiDlaTegoNeuronu()
	{	
		potecialMembranowy();
		
		double B = 10;
		double fx = 1/(1+Math.pow(2.718281828459, -B*pm));
		fx = B*fx*(1-fx);
				
		return  fx;

	}

}
